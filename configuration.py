import yaml
from helpers import *
import logging

import detect
import evaluate
import act
import rules

class Configuration:
    def __init__(self, file):
        try:
            self.load_from_file(file)
        except KeyError as e:
            raise ConfigException("Config missing required value") from e

    # load an enum value from location called name
    def load_enum(self, location, name):
        try:
            return getattr(location, name)
        except AttributeError as e:
            raise UnknownClassException("Config contains unknown constant {}.{}".format(location, c_name)) from e

    # load a class from a [name, args] pairing, from location
    def load_class(self, location, source):
        if isinstance(source, str):
            c_name = source
            c_args = []
        else:
            c_name = source[0]
            if len(source) > 1:
                c_args = source[1]
            else:
                c_args = []

        if not isinstance(c_args,list):
            c_args = [c_args]

        try:
            cls = getattr(location, c_name)
        except AttributeError as e:
            raise UnknownClassException("Config contains unknown class {}.{}".format(location, c_name)) from e
        
        try:
            return cls(*c_args)
        except TypeError as e:
            raise IncorrectArgumentsException("Config specified incorrect arguments {} (gave {})".format(c_name, c_args)) from e

    # load a value with a default
    def load_value(self, location, source, default="NODEFAULT"):
        try:
            return source[location]
        except KeyError:
            if default is "NODEFAULT":
                raise IncorrectArgumentsException("Missing configuration value {}".format(location))
            else:
                return default

    # load the rules from the config file
    def load_from_file(self, file):
        self.file = file

        with open(file,'r') as f:
            config = yaml.safe_load(f.read())

        self.settings = {
            'fallback': self.load_value('fallback', config),
            
            'log_level': self.load_value('log_level', config, logging.INFO),
            
            'report_id_path': self.load_value('report_id_path', config),
            'log_file_location': self.load_value('log_file_location', config),

            'mode': self.load_value('mode', config),

            'port': self.load_value('port', config, None),

            'input_folder': self.load_value('input_folder', config, None),
            'output_folder': self.load_value('output_folder', config, None)
        }

        self.detect_rules = []
        for rule in config['detect']:
            path = self.load_value('path', rule, "All")

            target = self.load_class(rules.detect, path)
            test = self.load_class(rules.detect, rule['test'])
            content = rule['type']

            self.detect_rules += [detect.DetectionRule(target, test, content)]

        self.evaluate_rules = []
        for rule in config['evaluate']:
            content = rule['type']
            test = self.load_class(rules.evaluate, rule['test'])
            action = rule['action']
            flags = self.load_value('flags', rule, [])
            
            self.evaluate_rules += [evaluate.EvaluationRule(content, test, action, flags)]

        self.act_rules = []
        for rule in config['act']:
            action = rule['action']
            method = self.load_class(rules.act, rule['method'])
            
            self.act_rules += [act.ActionRule(action, method)]

            