# RTM

The RunTimeMonitor inspects JSON-formatted dictionaries for content that
contravenes policy and takes appropriate action. This is performed in three 
stages:

*Detect*: The RTM first scans elements of the dictionary for content that 
matches specified types, and creates a list of detected content, such as email 
addresses or IPs.

*Evaluate*: The RTM evaluates each piece of content to determine if it is a 
violation of the policy. For example, email addresses can be tested against a 
blacklist of domains. Each piece of content results in a potential action to be 
taken.

*Act*: The RTM performs the actions determined by the previous step, modifying 
the dictionary as approriate and returning the result. This may include 
returning a null result.

# Usage

## Running

Use `pipenv install` to install dependencies and `pipenv run python core.py` 
to run the RTM. Alternatively, install the dependencies manually and run 
`python core.py`.

The RTM accepts alerts, in JSON format, and processes them according to its 
ruleset. If the alert is not dropped, it is returned in a (potentially 
redacted) form. If the alert is dropped, a null response is sent (or, in FILE 
mode, no file is outputted). If there is an error during processing, the 
returned JSON object will contain one key, `error`.

The RTM can be run in three modes, as specified in the config file (as `mode`):

- `SOCKET`: awaits connections and treats incoming messages as alerts, 
replying with the appropriate response.
- `FILE`: watch the input folder (specified with `input_folder`) for files, 
treating each as an alert, and output the results to the output folder 
(specified with `output_folder`). Input and output folders must be empty, and 
must be different.
- `STDIN`: Read alerts from STDIN and output to STDOUT.

## Docker

Build the docker container with `docker build -t protective-rtm .` and run it 
with `docker run -d protective-rtm`.

You should additionally either enable the relevant port (for SOCKET mode), 
using `docker run -p 10000 -d protective-rtm`, or mount volumes for the input 
and output folders (for FILE mode).

# Configuration

Configuration files can be found in the `conf` folder. The configuration file 
can be specified with the `-c` option.

Configuration files are in yaml format.

## Detect Rules

Detect rules specify the kind of content to look for, and what type to 
associate with each detection.

Each rule has the following options:

- `path`: The path targeter to use. 
    - Determines which parts of the JSON object will be tested with this rule.
    - Defaults to All, which matches any path.
    - Targeters are from `rules/rules_detect.py`
- `test`: The content tester to use.
    - Determines if a given piece of content should trigger a detection.
    - This is a list containing two elements, the name of the test and the 
    arguments of the test.
    - If the test has no arguments, this can just be the name.
    - Tests are from `rules/rules_detect.py`
- `type`: The label to give content found by this rule
    - Determines which Evaluate rules are applied.
    - Evaluate rules with this label will be applied to detections from this 
    rule.

## Evaluate Rules

Evaluate rules specify what to do with content detections.

Each rule has the following options:

- `type`: The label to apply this test to
    - This should match the `type` of the relevant Detect rules.
    - Any detections from Detect rules of this `type` will be evaluated by 
    this rule.
- `test`: The test to apply to this content. 
    - This is a list containing two elements, the name of the test and the 
    arguments of the test.
    - If the test has no arguments, this can just be the name.
    - Tests are from `rules/rules_evaluate.py`.
- `act`:  The action to perform on content that matches.
    - Possible actions are:
        - `KEEP_IDEA`: Keep the IDEA as-is and return it unmodified.
        - `ERASE_CONTENT`: Erase the specific content that was detected.
        - `DROP_IDEA`: Drop the whole IDEA and return nothing.
        - `REPORT_IDEA`: Report the detection (and keep the IDEA)

## Example

For instance, to create a rule that looks for the text "REMOVE_ME" anywhere 
and removes it, we need a Detect and an Evalute rule.

We use a Detect rule that uses `TestRegex` to look for the text "REMOVE_ME" 
and labels it as "remove_text":

```
 - test:
    - TestRegex
    - REMOVE_ME
   type: remove_text
```

And an Evaluate rule, which applies the `Always` test (i.e. always evalutes 
to true for any detection) and applies the action `ERASE_CONTENT`:

```
 - type: remove_text
   test: Always
   act:  ERASE_CONTENT
```

The result of this is, for example:

- Input: `{"test":"REMOVE_ME"}`
- Output: `{"test": "[removed]"}`