FROM python:3
ADD . /
RUN pip install pipenv
RUN pipenv install --system --deploy --ignore-pipfile
CMD [ "python", "./core.py", "-c", "./conf/protective.yaml"]