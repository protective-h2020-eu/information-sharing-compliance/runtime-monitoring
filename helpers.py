from collections import namedtuple
from enum import Enum
import os

ROOT_FOLDER = os.path.dirname(__file__)

def get_path(relative_path):
    return os.path.join(ROOT_FOLDER, relative_path)

# exceptions
class RTMException(Exception): pass

class DetectException(RTMException): pass
class UnknownTypeException(DetectException): pass

class EvaluateException(RTMException): pass
class RegexError(EvaluateException): pass

class ConfigException(RTMException): pass
class UnknownClassException(ConfigException): pass
class IncorrectArgumentsException(ConfigException): pass

# cross-component data tuples
class DetectedContent(namedtuple('DetectedContent', 'path content type')):
    pass

class EvaluationResult(namedtuple('EvaluationResult', 'detection action flags')):
    pass

# flags
class EvaluationFlag(Enum):
    # do not report the action being taken in the log
    SILENT = "SILENT"


# case insensitive matching
def _key_match(k1, k2):
    try: 
        k1 = k1.lower()
    except Exception:
        pass

    try: 
        k2 = k2.lower()
    except Exception:
        pass

    return k1 == k2

def find_value(nested_object, path_list, action):
    field = nested_object
    for k, next_path in enumerate(path_list):
        try:
            if k == len(path_list) - 1:
                return action(field, next_path)
            else:
                field = field[next_path]
                continue
        except KeyError:
            for subfield in field:
                if _key_match(subfield, next_path):
                    if k == len(path_list) - 1:
                        return action(field, subfield)
                    else:
                        field = field[subfield]
                        continue
            else:
                field[next_path] = {}
                field = field[next_path]

def drop_value(nested_object, path_list):
    def action(field, subfield):
        del field[subfield]
    find_value(nested_object, path_list, action)

def get_value(nested_object, path_list):
    def action(field, subfield):
        return field[subfield]
    return find_value(nested_object, path_list, action)

def set_value(nested_object, path_list, value):
    def action(field, subfield):
        field[subfield] = value
    find_value(nested_object, path_list, action)
