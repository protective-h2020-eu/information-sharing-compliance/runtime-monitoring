from collections import namedtuple
import logging

from helpers import *

class EvaluationTest:
    # test a piece of content to see if it passes the test
    def evaluate(self, content):
        return True

class EvaluationRule(namedtuple('EvaluationRule', 'type test action flags')):
    def evaluate(self, detection):
        result = self.test.evaluate(detection.content)

        if result:
            return EvaluationResult(detection, self.action, self.flags)
        else:
            return None

class EvaluateComponent:
    def __init__(self, config):
        self.config = config
        self.rules = config.evaluate_rules

        logging.info("Initialised EvaluateComponent with {} rules".format(len(self.rules)))

    def evaluate(self, detections):
        results = [
            r.evaluate(detection)
            for detection in detections
            for r in self.rules 
            if r.type == detection.type
        ]

        return [r for r in results if r]
