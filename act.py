from enum import Enum
import logging
import copy
from collections import namedtuple

from helpers import *

class Action:
    def act(self, idea, evaluation):
        return idea

class ActionRule(namedtuple('ActionRule', 'action method')):
    def act(self, idea, evaluation):
        return self.method.act(idea, evaluation)

class ActComponent:
    def __init__(self, config=None):
        self.config = config
        self.rules = self.config.act_rules
        
        logging.info("Initialised ActComponent with {} rules".format(len(self.rules)))

    def act(self, idea, evaluations):
        modified_idea = copy.deepcopy(idea)
        for e in evaluations:
            for r in self.rules:
                if r.action == e.action:
                    modified_idea = r.act(modified_idea, e)
                    if not modified_idea:
                        break
        return modified_idea