# Fallback mode for when alerts fail to be handled
# can either be DROP or KEEP
fallback: KEEP

# The operation mode, which should be STDIN, FILE, or SOCKET
mode: TESTING

# The path of the value to use as the ID for logging
report_id_path: ID

# Location of the log file for reporting
log_file_location: test/logs/test_log.txt

# Types to replace erased content with
# Content will be replaced with the first value that matches its type
# The first value is used if no match is found
erase_values:
  - "[removed]"
  - 0 

#
# Detect rules specify the kind of content to look for.
# Each rule has the following options:
# 
#   path: The path targeter to use. (from rules/rules_detect.py). 
#         Defaults to All, which matches any path.
#   test: The content tester to use This is a list containing two elements, 
#         the name of the test and the arguments of the test. If no arguments 
#         are needed, this can just be the name.
#         Tests are from rules/rules_detect.py
#   type: The label to give content found by this rule
#

detect:
 - test: TestEmail
   type: email
 - test: TestIP
   type: ip
 - test: TestCardNumber
   type: bankcard
 - test: TestPrivateKey
   type: privatekey

 - path:
    - Starts
    - ["dropme"]
   test: TestAnything
   type: dropme

 - path:
    - Starts
    - ["test_detect"]
   test: TestEmail
   type: eraseme
 - path:
    - Starts
    - ["test_detect"]
   test: TestIP
   type: eraseme
 - path:
    - Starts
    - ["test_detect"]
   test: TestCardNumber
   type: eraseme
 - path:
    - Starts
    - ["test_detect"]
   test: TestPrivateKey
   type: eraseme

 - path:
    - Starts
    - ["AnInteger"]
   test: TestAnything
   type: eraseme

 - path:
    - Starts
    - ["VerboseErase"]
   test: TestAnything
   type: eraseme
 - path:
    - Starts
    - ["SilentErase"]
   test: TestAnything
   type: eraseme_silent

 - path:
    - Starts
    - ["DropField"]
   test: TestAnything
   type: dropfield

 - path:
    - Starts
    - ["Timeliness"]
   test: TestDateTime
   type: timeliness

 - path:
    - Starts
    - ["TimeOfDay"]
   test: TestDateTime
   type: timeofday

 - path:
    - Starts
    - ["MarkThisUp"]
   test: TestAnything
   type: apply_markup

 - path:
    - Starts
    - ["TLPColour"]
   test: TestAnything
   type: set_colour

# 
# Evaluate rules specify what to do with content detections
# Each rule has the following options:
#
#   type: The label to apply this test to (as above)
#   test: The test to apply to this content. This is a list containing two 
#         elements, the name of the test and the arguments of the test. If no 
#         arguments are needed, this can just be the name. 
#         Tests are from rules/rules_evaluate.py
#   act:  The action to perform on content that matches, from act.py
# 

evaluate:
 - type: email
   test:
    - RegexBlacklist
    - [['.*@innocent.com']]
   action:  ERASE_CONTENT
   
 - type: ip
   test:
    - FileRegexBlacklist
    - test/ip_regex_blacklist.txt
   action:  ERASE_CONTENT

 - type: bankcard
   test: Always
   action:  ERASE_CONTENT

 - type: privatekey
   test: Always
   action:  ERASE_CONTENT

 - type: dropme
   test: Always
   action:  DROP_IDEA
   
 - type: dropfield
   test: Always
   action:  DROP_FIELD

 - type: eraseme
   test: Always
   action:  ERASE_CONTENT

 - type: eraseme_silent
   test: Always
   action:  ERASE_CONTENT
   flags: [SILENT]

 - type: ip
   test:
    - RegexBlacklist
    - [["123.*"]]
   action: IP_ANONYMISE

 - type: email
   test:
    - RegexBlacklist
    - [['.*@internal.com']]
   action: EMAIL_ANONYMISE

 - type: timeliness
   test:
    - TestTimeliness
    - [100]
   action: ERASE_CONTENT

 - type: timeofday
   test:
    - TestTimeOfDay
    - [0, 43200]
   action: REPLACE_AS_MORNING

 - type: timeofday
   test:
    - TestTimeOfDay
    - [43200, 86400]
   action: REPLACE_AS_EVENING

 - type: apply_markup
   test: Always
   action: MARK_UP

 - type: set_colour
   test: 
    - RegexBlacklist
    - [["red"]]
   action: TLP_RED
 - type: set_colour
   test: 
    - RegexBlacklist
    - [["amber"]]
   action: TLP_AMBER
 - type: set_colour
   test: 
    - RegexBlacklist
    - [["green"]]
   action: TLP_GREEN
 - type: set_colour
   test: 
    - RegexBlacklist
    - [["white"]]
   action: TLP_WHITE

act:
 - action: ERASE_CONTENT
   method: 
    - ActionErase
    - - ["[removed]", 0 ]

 - action: DROP_IDEA
   method: ActionDrop

 - action: DROP_FIELD
   method: ActionDropField

 - action: KEEP
   method: ActionKeep

 - action: REPORT
   method: ActionReport

 - action: MARK_UP
   method:
     - ActionMarkUp
     - - 'NewValue'
       - 'NEW'

 - action: MARK_UP
   method:
     - ActionMarkUp
     - - ['Parent','Child']
       - 'NEW2'

 - action: IP_ANONYMISE
   method:
    - ActionReplace
    - - '(\d{1,3})\.(\d{1,3}).*'
      - '\g<1>.\g<2>.x.x'

 - action: EMAIL_ANONYMISE
   method:
    - ActionReplace
    - - '[^@]*@(.*)'
      - 'x@\g<1>'

 - action: REPLACE_AS_MORNING
   method:
    - ActionReplace
    - - '.*'
      - 'Morning'

 - action: REPLACE_AS_EVENING
   method:
    - ActionReplace
    - - '.*'
      - 'Evening'

 - action: TLP_RED
   method:
    - ActionTLP
    - - 'tlp_value'
      - 'RED'
 - action: TLP_AMBER
   method:
    - ActionTLP
    - - 'tlp_value'
      - 'AMBER'
 - action: TLP_GREEN
   method:
    - ActionTLP
    - - 'tlp_value'
      - 'GREEN'
 - action: TLP_WHITE
   method:
    - ActionTLP
    - - 'tlp_value'
      - 'WHITE'