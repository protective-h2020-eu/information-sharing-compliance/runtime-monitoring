import json
import time

import core

def test_on_file(rtm, file):
   with open(file,'r') as f:
      return rtm.handle_message(f.read())

def perform_test(rtm):
   TRIALS = 10
   ROUNDS = 1000

   print("Beginning performance tests ({} trials, {} rounds):".format(TRIALS, ROUNDS))

   # run a test JSON through the RTM a number of times
   for i in range(0,TRIALS):
      last_time = time.time()
      for r in range(0,ROUNDS):
         if r % 50 == 0:
            progress = round(100 * r / ROUNDS)
            print("\r", end="")
            print("    Trial {}: In progress ({}%)".format(i + 1, progress), end="", flush=True)
         test_on_file(rtm, "test/tests/performance.json")

      taken = time.time() - last_time

      total = round(taken*1000)/1000
      per = round(taken/ROUNDS*1000000)/1000

      print("\r", end="")
      print("    Trial {}: Complete. Total {}s ({}ms per alert)".format(i + 1, total, per))


   print("Performance test complete.")

