import json
import logging
import core

import os
from helpers import *

def test_on_file(rtm, file):
   with open(file,'r') as f:
      return rtm.handle_message(f.read())

def get_log(rtm):
   try:
      with open(get_path(rtm.conf.settings['log_file_location']),"r") as f:
         return f.read()
   except FileNotFoundError:
      print(get_path(rtm.conf.settings['log_file_location']))
      return ""

def test(name, rtm, file, assertions):
   # initialise
   report_log_file = get_path(rtm.conf.settings['log_file_location'])
   try:
      with open(report_log_file, "w") as f:
         f.write("") # deleting the file seems to upset the logger
   except FileNotFoundError:
      pass

   logging.info("Running test {} ({})".format(name,file))
   old_level = logging.getLogger().level
   logging.getLogger().setLevel(logging.CRITICAL)

   # load file
   result_raw = test_on_file(rtm, file)
   try:
      result = json.loads(result_raw)
   except TypeError:
      result = None

   # check result
   try:
      assert assertions(result)
   except AssertionError:
      logging.critical("!\t Test failed ({})".format(name))
      return False
   except Exception as e:
      logging.critical("!\t Test failed (in an unexpected way) ({})".format(name))
      print(result)
      raise e
      return False
   finally:
      logging.getLogger().setLevel(old_level)

   logging.info("\t Test passed ({})".format(name))
   return True

def perform_test(rtm):
   test_results = [
      # check for erasing/keeping
      test("Erase", rtm, "test/tests/erase.json", lambda result: (
         "error" not in result and
         result["erase_email"] == "[removed]" and
         result["leave_email"] != "[removed]" and
         result["erase_ip"] == "[removed]" and
         result["leave_ip"] != "[removed]" and
         result["erase_card"] == "[removed]" and
         "secret" not in result["erase_key"]
      )),

      # check for drop
      test("Drop", rtm, "test/tests/drop.json", lambda result: (
         result is None
      )),

      # check for malformed json handling
      test("Malformed JSON", rtm, "test/tests/malformed.json", lambda result: (
         "error" in result
      )),

      # check for handling a list
      test("Parsing Lists", rtm, "test/tests/list.json", lambda result: (
         all([r == "[removed]" for r in result["erase_all"]])
      )),
      
      # check for json with differently-cased ID
      test("Case Insensitivity", rtm, "test/tests/lowercase_id.json", lambda result: (
         result["ErAsE_EmAiL"] == "[removed]"
      )),

      # check for detect rules
      test("Detect Rules", rtm, "test/tests/detect.json", lambda result: (
         result["test_detect"]["ip"] == "[removed]" and
         result["test_detect"]["email"] == "[removed]" and
         result["test_detect"]["bankcard"] == "[removed]" and
         "secret" not in result["test_detect"]["privatekey"]
      )),

      # check that integers can be erased (fix for reported issue)
      test("Erase Non-Strings", rtm, "test/tests/replace_types.json", lambda result: (
         result["AnInteger"] == 0
      )),

      # check that errors are sent through
      test("Error Fallback", rtm, "test/tests/error.json", lambda result: (
         "IsStillHere" in result
      )),

      # check that non-silent rules are reported
      test("Non-Silent Rules", rtm, "test/tests/report.json", lambda result: (
         result["VerboseErase"] == "[removed]" and
         "ShouldBeReported" in get_log(rtm)
      )),

      # check that silent rules are not reported
      test("Silent Rules", rtm, "test/tests/silent.json", lambda result: (
         result["SilentErase"] == "[removed]" and
         "ShouldNotBeReported" not in get_log(rtm)
      )),

      # check that drop field works
      test("Drop Field", rtm, "test/tests/drop_field.json", lambda result: (
         "DropField" not in result
      )),

      # check that drop field works
      test("Anonymisation", rtm, "test/tests/anonymise.json", lambda result: (
         result["ip"] == "123.1.x.x" and
         result["email"] == "x@internal.com"
      )),

      # check that timeliness works
      test("Timeliness", rtm, "test/tests/time.json", lambda result: (
         result["Timeliness"]["past"] == "[removed]" and
         result["Timeliness"]["future"] != "[removed]"
      )),

      # check that time of day works
      test("Time Of Day", rtm, "test/tests/time_of_day.json", lambda result: (
         result["TimeOfDay"]["morning"] == "Morning" and
         result["TimeOfDay"]["evening"] == "Evening"
      )),

      # check that marking up works
      test("Mark Up", rtm, "test/tests/markup.json", lambda result: (
         result["NewValue"] == "NEW" and
         result["Parent"]["Child"] == "NEW2" 
      )),

      # check that TLP mark up works
      test("Mark Up TLP", rtm, "test/tests/tlp.json", lambda result: (
         result["tlp_value"] == "RED"
      )),

      # check that TLP mark up works
      test("TLP Non-overwrite", rtm, "test/tests/tlp_overwrite.json", lambda result: (
         result["tlp_value"] == "AMBER"
      )),
   ]

   if all(test_results):
      print("\n\n\tAll tests passed\n\n")
      return True
   else:
      print("\n\n\tOne or more tests failed!\n\n")
      return False
