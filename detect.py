import logging
import re
from collections import namedtuple

#from idea.lite import Idea

from helpers import *

class DetectionTest:
    def __init__(self):
        pass

    def detect(self, content):
        return [content]

class DetectionTarget:
    def __init__(self):
        pass

    def matches(self, path):
        return True

class DetectionRule(namedtuple('DetectionRule', 'target test type')):
    def matches(self, path):
        return self.target.matches(path)

    def detect(self, path):
        return self.test.detect(path)

class DetectComponent:
    def __init__(self, config):
        self.detect_types = [int, str, bool, float]
        self.config = config
        self.rules = config.detect_rules

        logging.info("Initialised DetectComponent with {} rules".format(len(self.rules)))

    def _match_rules(self, path):
        return [r for r in self.rules if r.matches(path)]

    def _detect(self, subobj, path = []):
        if type(subobj) in self.detect_types:
            matching_rules = self._match_rules(path)
            detections = []
            for rule in matching_rules:
                detections += [
                    DetectedContent(path, content, rule.type)
                    for content in rule.detect(subobj)
                ]
            return detections

        if isinstance(subobj, dict):
            return [
                d 
                for i,v in subobj.items()
                for d in self._detect(v, path+[str(i)]) 
            ]
        if isinstance(subobj, list):
            return [
                d
                for i,o in enumerate(subobj)
                for d in self._detect(o, path+[i])
            ]

        raise UnknownTypeException(
            "Tried to run detection on object of type {} (path {})".format(
                type(subobj), path
            )
        )


    def detect(self, idea):
        return self._detect(dict(idea))