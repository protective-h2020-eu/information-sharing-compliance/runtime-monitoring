import logging
import base64
from datetime import datetime

from helpers import *

class ReportComponent:
    def __init__(self, config=None):
        self.config = config

        logging.info("Initialised ReportComponent")

        formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')

        handler = logging.FileHandler(self.config.settings['log_file_location'])
        handler.setFormatter(formatter)

        self.logger = logging.getLogger("ReportComponent ({})".format(id(self)))
        self.logger.setLevel(logging.INFO)
        self.logger.addHandler(handler)
        self.logger.propagate = False

    def report(self, idea, evaluations, result):
        report_id_path = self.config.settings['report_id_path'].split(".")
        try:
            report_id = get_value(idea, report_id_path)
        except KeyError:
            logging.warn("Reporting on IDEA with unspecified ID (path: {})".format(self.config.settings['report_id_path']))
            report_id = "Unknown"

        for e in evaluations:
            if not EvaluationFlag.SILENT.value in e.flags:
                self.logger.info(self.encode_log(report_id, e))

    def encode_log(self, report_id, evaluation):
        enc_content = base64.b64encode(
            bytes(str(evaluation.detection.content), 'ascii')
        ).decode('ascii')
        return "{} {} {} {} {}".format(
            report_id, 
            ".".join([str(s) for s in evaluation.detection.path]),
            enc_content,
            evaluation.detection.type,
            evaluation.action
        )

    def decode_log(self, log_line):
        parts = log_line.split(" ")
        datestamp, timestamp = parts[0:2]
        evaluation_id, path, enc_content, det_type, action = parts[3:]

        datetimestamp = datetime.strptime(datestamp + " " + timestamp, "%Y-%m-%d %H:%M:%S,%f")
        content = base64.b64decode(enc_content).decode('ascii')
        
        return [datetimestamp, evaluation_id, path, content, det_type, action]