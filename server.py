import socket 
import logging
from threading import Thread 
from socketserver import ThreadingMixIn 

# Multithreaded Python server : TCP Server Socket Thread Pool
class ClientHandler(Thread): 
    def __init__(self,conn,ip,port): 
        Thread.__init__(self) 
        self.ip = ip 
        self.port = port 
        self.conn = conn

        self.maintain_connection = True

        logging.debug("Connection made to {}".format(ip))
 
    def stop_connection(self):
        self.maintain_connection = False

    def run(self): 
        self.conn.send(self.initial_message())

        while self.maintain_connection: 
            data = self.conn.recv(2048) 
            if data == 'exit' or not data:
                break
            response = self.handle_message(data.decode())
            self.conn.send(response.encode())
        self.conn.close()

    #
    #   Server behaviour
    #

    def initial_message(self):
        return b""

    def handle_message(self, data):
        return data

class Server:
    def __init__(self, ip, port, client_handler):
        # Multithreaded Python server : TCP Server Socket Program Stub
        self.TCP_IP = ip
        self.TCP_PORT = port 
        self.BUFFER_SIZE = 1024 

        self.tcpServer = socket.socket(socket.AF_INET, socket.SOCK_STREAM) 
        self.tcpServer.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1) 
        self.tcpServer.bind((self.TCP_IP, self.TCP_PORT)) 
        self.threads = [] 

        self.client_handler = client_handler
 
    def listen(self):
        try:
            while True: 
                self.tcpServer.listen(50) 
                logging.info("Server listening on port {}".format(self.TCP_PORT))
                (conn, (ip,port)) = self.tcpServer.accept() 
                newthread = self.client_handler(conn,ip,port) 
                newthread.start() 
                self.threads.append(newthread) 
        finally:
            for t in self.threads: 
                t.join() 
