import re

import act
from helpers import *

class ActionKeep(act.Action):
    def act(self, idea, evaluation):
        return idea

class ActionDrop(act.Action):
    def act(self, idea, evaluation):
        return None

class ActionErase(act.Action):
    def __init__(self, replace_with):
        self.replace_with = replace_with

    def get_replacement(self, content):
        for replacement in self.replace_with:
            if isinstance(replacement, type(content)):
                return replacement
        return self.replace_with[0] # default to the first

    def act(self, idea, evaluation):
        path = evaluation.detection.path
        content = evaluation.detection.content
        value = get_value(idea,path)
        replacement = self.get_replacement(content)
        try:
            new_value = value.replace(content, replacement)
        except AttributeError:
            # if the type of value doesn't allow replacing, just
            # substitute the whole field for the new value
            new_value = replacement

        set_value(idea, path, new_value)

        return idea

class ActionDropField(act.Action):
    def act(self, idea, evaluation):
        path = evaluation.detection.path
        content = evaluation.detection.content
        value = drop_value(idea,path)

        return idea

class ActionReport(act.Action):
    def act(self, idea, evaluation):
        logging.warning('Reporting action not implemented yet')

        return idea

class ActionReplace(act.Action):
    def __init__(self, regex_find, regex_replace):
        self.regex_find = re.compile(regex_find)
        self.regex_replace = regex_replace

    def act(self, idea, evaluation):
        path = evaluation.detection.path
        content = evaluation.detection.content

        value = get_value(idea,path)
        new_value = self.regex_find.sub(self.regex_replace, value)
        set_value(idea, path, new_value)
        return idea

class ActionMarkUp(act.Action):
    def __init__(self, path, value):
        self.path = [path] if isinstance(path,str) else path
        self.value = value

    def act(self, idea, evaluation):
        set_value(idea, self.path, self.value)
        return idea

class ActionTLP(ActionMarkUp):
    colours = ["WHITE","GREEN","AMBER","RED"]
    def __init__(self, path, value):
        if value not in ActionTLP.colours:
            raise IncorrectArgumentsException("{} is not a TLP colour ({})".format(value, ActionTLP.colours))
        super().__init__(path, value)

    def act(self, idea, evaluation):
        old_value = get_value(idea, self.path)
        try:
            if ActionTLP.colours.index(old_value) > ActionTLP.colours.index(self.value):
                return idea
        except ValueError:
            pass
        set_value(idea, self.path, self.value)
        return idea