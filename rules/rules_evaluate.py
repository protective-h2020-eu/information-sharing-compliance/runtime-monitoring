import re
import logging
import strict_rfc3339 # for TestTimeliness
import time           # for TestTimeliness

import evaluate
from helpers import *

class Always(evaluate.EvaluationTest):
    pass

class TestBlacklist(evaluate.EvaluationTest):
    def __init__(self, blacklist):
        self.blacklist = blacklist

    def evaluate(self, content):
        return content in self.blacklist

class RegexBlacklist(evaluate.EvaluationTest):
    def __init__(self, blacklist):
        try:
            self.blacklist = [re.compile(b) for b in blacklist]
        except re.error as e:
            raise RegexError("Regex failed to compile") from e

    def evaluate(self, content):
        for b in self.blacklist:
            if b.match(content): return True
        return False

class FileBlacklist(TestBlacklist):
    def __init__(self, filename, sep="\n"):
        with open(filename,'r') as f:
            blacklist = f.read().split("\n")
        super().__init__(blacklist)

class FileRegexBlacklist(RegexBlacklist, FileBlacklist):
    def __init__(self, filename, sep="\n"):
        FileBlacklist.__init__(self, filename, sep)
        RegexBlacklist.__init__(self, self.blacklist)

    def evaluate(self, content):
        return RegexBlacklist.evaluate(self, content)

class TestTimeliness(evaluate.EvaluationTest):
    """Evaluates based on the time between the value and now

    args:
        max_seconds: how old the datestamp has to be before triggering this rule [number]
        default: whether to trigger on unformatted dates or not [boolean]"""

    def __init__(self, max_seconds, default=True):
        self.max_seconds = max_seconds
        self.default = default

    def evaluate(self, content):
        try:
            detected_time = strict_rfc3339.rfc3339_to_timestamp(content)
        except strict_rfc3339.InvalidRFC3339Error:
            logging.warning("Tried to evaluate a misformatted timestamp ({})".format(content))
            return self.default

        now_time = time.time()

        if not detected_time:
            return self.default
        return now_time - detected_time > self.max_seconds

class TestTimeOfDay(evaluate.EvaluationTest):
    """Evaluates based on the time of day

    args:
        period_start: When (in seconds from midnight) to start the period
        period_end: When (in seconds from midnight) to end the period
        act_in_period: If true, evaluates to true when the time is during the period (otherwise when outside)
        default: whether to trigger on unformatted dates or not [boolean]"""

    def __init__(self, period_start, period_end, act_in_period=True, default=True):
        self.period_start = period_start
        self.period_end = period_end
        self.act_in_period = act_in_period
        self.default = default

    def evaluate(self, content):
        try:
            detected_time = strict_rfc3339.rfc3339_to_timestamp(content)
        except strict_rfc3339.InvalidRFC3339Error:
            logging.warning("Tried to evaluate a misformatted timestamp ({})".format(content))
            return self.default
        
        detected_seconds_in_day = detected_time % (60 * 60 * 24)

        if (detected_seconds_in_day >= self.period_start and
            detected_seconds_in_day <= self.period_end):
           return self.act_in_period
        return not self.act_in_period
