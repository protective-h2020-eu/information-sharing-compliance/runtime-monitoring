import re

import strict_rfc3339 # for TestTimeliness
import time

import detect

#
#   Detection Targets
#   These are used by Detect rules (as path)
#   They determine which parts of the JSON input to test
#

class All(detect.DetectionTarget):
    pass

class Starts(detect.DetectionTarget):
    def __init__(self, starts):
        if not isinstance(starts, list):
            starts = [starts]
        self.starts = starts

    def matches(self, path):
        for i, part in enumerate(path[:len(self.starts)]):
            if self.starts[i] == "*":
                continue
            if self.starts[i].lower() == part.lower():
                continue
            return False
        return True

#
#   Detection Tests
#   These are used by Detect rules (as test)
#   They return a list of detections that pass their test.
#

class TestRegex(detect.DetectionTest):
    def __init__(self, rules, regex_flags=0):
        if not isinstance(rules, list):
            rules = [rules]
        self.rules = [re.compile(r, regex_flags) for r in rules]

    def detect(self, content):
        return [
            detection
            for rule in self.rules
            for detection in rule.findall(str(content))
        ]

# Always detects anything, returns the whole thing as a detection
class TestAnything(detect.DetectionTest): pass

class TestEmail(TestRegex):
    def __init__(self):
        super().__init__(r"(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$)")

class TestIP(TestRegex):
    def __init__(self):
        super().__init__([
            r"\b[0-9]+(?:\.[0-9]+){3}\b",                       # ipv4
            #r"\b([0-9a-fA-F]{0,4}:){2,7}[0-9a-fA-F]{0,4}\b",    # ipv6 (loose)
        ])

class TestIPRange(TestRegex):
    def __init__(self):
        super().__init__([
            r"\b[0-9]+(?:\.[0-9]+){3}/[0-9]{1,3}\b",                # x.x.x.x/24
            r"\b[0-9]+(?:\.[0-9]+){3} ?\- ?[0-9]+(?:\.[0-9]+){3}\b" # x.x-x.x
        ])

class TestCardNumber(TestRegex):
    def __init__(self):
        super().__init__([
            # regex from https://www.regular-expressions.info/creditcard.html
            r"\b4[0-9]{12}(?:[0-9]{3})?\b",          # Visa
            r"\b(?:5[1-5][0-9]{2}|222[1-9]|22[3-9][0-9]|2[3-6][0-9]{2}|27[01][0-9]|2720)[0-9]{12}\b",
            r"\b3[47][0-9]{13}\b",                   # American Express
            r"\b3(?:0[0-5]|[68][0-9])[0-9]{11}\b",   # Diners Club
            r"\b6(?:011|5[0-9]{2})[0-9]{12}\b",      # Discover
            r"\b(?:2131|1800|35\d{3})\d{11}\b"      # JCB

        ])

class TestPrivateKey(TestRegex):
    def __init__(self):
        super().__init__([
            r"-----BEGIN [A-Z0-9 ]*-----(.*)-----END ?[A-Z0-9 ]*-----" # private key files
        ], re.DOTALL | re.MULTILINE)

class TestDateTime(TestRegex):
    """Detects datetimes based on regex given in IDEA schema"""
    
    def __init__(self):
        super().__init__(
            r"^[0-9]{4}-[0-9]{2}-[0-9]{2}[Tt ][0-9]{2}:[0-9]{2}:[0-9]{2}(?:\\.[0-9]+)?(?:[Zz]|(?:[+-][0-9]{2}:[0-9]{2}))?$"
        )


# this overmatches to almost anything
#class TestPhoneNumbers(TestRegex):
#    def __init__(self):
#        super().__init__([
#            # regex from https://stackoverflow.com/questions/123559/a-comprehensive-regex-for-phone-number-validation
#            # slightly modified to only make one capture group
#            r"(?:(?:\(?(?:00|\+)(?:[1-4]\d\d|[1-9]\d?)\)?)?[\-\.\ \\\/]?)?(?:(?:\(?\d{1,}\)?[\-\.\ \\\/]?){1,})(?:[\-\.\ \\\/]?(?:#|ext\.?|extension|x)[\-\.\ \\\/]?(?:\d+))?", 
#        ])

#class TestNames(TestRegex):
#    pass

