import logging
import json
import socket
import argparse
import traceback
from sys import exit
from os import listdir, remove
from os.path import isfile, join
from time import sleep

from helpers import *

import configuration
import test
import server

import detect
import evaluate
import act
import report


class RunTimeMonitor:
    def __init__(self, conf):
        self.conf = conf

        self.detect = detect.DetectComponent(self.conf)
        self.evaluate = evaluate.EvaluateComponent(self.conf)
        self.act = act.ActComponent(self.conf)
        self.report = report.ReportComponent(self.conf)

    def process(self, idea):
        detections = self.detect.detect(idea)
        logging.debug("Found {} detections".format(len(detections)))
        for d in detections:
            logging.debug(d)
        
        evaluations = self.evaluate.evaluate(detections)
        logging.debug("Found {} evaluations".format(len(evaluations)))
        for e in evaluations:
            logging.debug(e)
        
        result = self.act.act(idea, evaluations)

        self.report.report(idea, evaluations, result)

        return result

    def handle_message(self, data):
        try:
            try:
                message = json.loads(data.strip())
            except ValueError:
                logging.warning("Failed to decode input JSON")
                return json.dumps({'error':'Failed to decode input'})

            # this is to test that the RTM is handling inputs at all
            # passing it a dict with __RTM_TEST_KEY__ always gets it replaced
            if "__RTM_TEST_KEY__" in message:
                message["__RTM_TEST_KEY__"] = "RunTimeMonitor Tested Successfully"
            if "__RTM_ERROR_KEY__" in message:
                raise Exception("This is a test Exception")

            response = self.process(message)

            if response:
                return json.dumps(response)
            else:
                return False

        except Exception as e:
            logging.error("Exception: {}".format(e))

            if self.conf.settings['fallback'] == "KEEP": 
                return data
            else:
                return json.dumps({'error':traceback.format_exc()})

#
# Handlers
#
# Each of these processes the alerts from a different source
#
class Handler:
    def __init__(self, config):
        self.config = config

    def listen(self):
        pass

#
#   RTMSocketHandler
#
#   Listens on a port for connections, replies to each with parsed
#   results
#
class RTMSocketHandler(Handler):
    def __init__(self, config, port):
        super().__init__(config)
        handler = lambda c, i, p, config=self.config: RTMClientHandler(c, i, p, config)
        self.server = server.Server('0.0.0.0', port, handler)

    def listen(self):
        self.server.listen()

# Created for each socket connection
class RTMClientHandler(server.ClientHandler):
    def __init__(self, conn, ip, port, config):
        super().__init__(conn, ip, port)
        self.rtm = RunTimeMonitor(config)

    def handle_message(self, data):
        return self.rtm.handle_message(data)


#
#   RTMTextHandler
#
#   Waits for STDIN and outputs to STDOUT
#
#
class RTMTextHandler(Handler):
    def __init__(self, config):
        super().__init__(config)
        self.rtm = RunTimeMonitor(self.config)

    def listen(self):
        while True:
            try:
                data = input()
            except EOFError:
                return
            except KeyboardInterrupt:
                return
            print(self.rtm.handle_message(data))

#
#   RTMFileHandler
#
#   Watches a folder, opening each file and treating it as an alert
#   Outputs to a second folder, each file as an alert
#
class RTMFileHandler(Handler):
    def __init__(self, config, in_folder, out_folder):
        super().__init__(config)
        self.rtm = RunTimeMonitor(self.config)
        self.in_folder = in_folder
        self.out_folder = out_folder

    def listen(self):
        while True: 
            sleep(1)

            input_files = [
                f for f in listdir(self.in_folder)
                if isfile(join(self.in_folder, f))
            ] 

            for filename in input_files:
                input_file = join(self.in_folder, filename)
                output_file = join(self.out_folder, filename)

                try:
                    with open(input_file,"r") as f:
                        content = f.read()
                    remove(input_file)
                    
                    reply = self.rtm.handle_message(content)
                    if reply:
                        with open(output_file, "w") as f:
                            f.write(reply)
                except PermissionError:
                    pass



if __name__ == "__main__":
    # arguments
    parser = argparse.ArgumentParser(description='Run the RunTimeMonitor.')

    #
    # Configuration
    #
    parser.add_argument("-c", "--config",
        help="Path to configuration file (defaults to conf/standard.yaml)")

    #
    # Tests
    #
    parser.add_argument("--test",
        help="Run the tests", 
        action="store_true")
    parser.add_argument("--performance",
        help="Run the performance test", 
        action="store_true")
    
    args = parser.parse_args()

    # logging
    streamhandler = logging.StreamHandler()
    streamhandler.setFormatter(logging.Formatter("{asctime} {name} {message}", style='{'))

    handler = logging.FileHandler("log.txt")
    handler.setFormatter(logging.Formatter("{asctime} {name} {message}", style='{'))

    logging.getLogger().addHandler(handler)
    logging.getLogger().addHandler(streamhandler)
    logging.getLogger().setLevel(logging.INFO)

    if args.test:
        config = configuration.Configuration("test/test.yaml")
        success = test.standard.perform_test(RunTimeMonitor(config))
        exit(0 if success else 1)
    elif args.performance:
        config = configuration.Configuration("test/test.yaml")
        test.performance.perform_test(RunTimeMonitor(config))
    else:
        args.config = args.config or "conf/standard.yaml"

        config = configuration.Configuration(args.config)

        loglevel = config.settings['log_level']
        logging.getLogger().setLevel(loglevel)
        mode = config.settings['mode']
        logging.info("Starting RTM in mode {}".format(mode))

        # run the handler
        if mode == "STDIN":
            handler = RTMTextHandler(config)
            handler.listen()

        elif mode == "FILE":
            if not config.settings['input_folder']:
                logging.critical("No input_folder specified in file mode (config {}).".format(args.config))
            elif not config.settings['output_folder']:
                logging.critical("No output_folder specified in file mode (config {}).".format(args.config))
            elif listdir(config.settings['input_folder']):
                logging.critical("Input folder did not start empty, stopping to ensure no files are deleted.")
            elif config.settings['input_folder'] == config.settings['output_folder']:
                logging.critical("Output and input folders cannot be the same, stopping.")
            else:
                handler = RTMFileHandler(config, config.settings['input_folder'], config.settings['output_folder'])
                handler.listen()

        elif mode == "SOCKET":
            handler = RTMSocketHandler(config, int(config.settings['port']))
            handler.listen()

        else:
            logging.critical("Unknown operation mode ({}) specified in config ({})".format(mode, args.config))
